## aperorama #2 _ 2.08.17 _ La Paillasse


### Salut !

Merci d’être venu-e-s hier à l’apérorama ! 👌🍷🍻🍻

voilà quelques liens vers les choses dont on avait causé hier :

- par rapport à l’histoire de Richard Stallman, et de l’anecdote de l’imprimante Xerox, il y a le livre de Sébastien Brocca, Utopie du logiciel libre, il y a la version PDF [ici](http://lepassagerclandestin.fr/fileadmin/assets/catalog/essais/Utopie_logiciel_libre__Broca__Le_passager_clandestin.pdf)
l’histoire Xerox est à la page 48

- P. avait parlé de [emojipedia](http://emojipedia.org/), le wiki des émojis et leurs significations

- au sujet du web qui se meurt, il y a un article dans [Usbek&Rica](https://usbeketrica.com/article/ce-forum-mondial-que-permet-internet-pourrait-avoir-disparu-dans-15-ans) qui le raconte plutôt bien, ainsi que la chronique [web is dead](http://www.novaplanet.com/radionova/78767/episode-le-web-est-mort-vive-le-web) de Christophe Payet sur radio nova, en juillet dernier.

- les licences, voir la [Creative Commons](http://creativecommons.fr/)

- la revue [Back Office](http://www.revue-backoffice.com/) dont parlait L. 

- L. parlait aussi de Franck Adebiaye, et c’est le cofondateur de [Velvetyne](http://www.velvetyne.fr/authors/frnk/) !

- et aussi, [prototypo](https://www.prototypo.io/)
et [metaflop](http://www.metaflop.com/modulator), pour moduler des typos à partir de trois autres typos qui, si je ne dis pas de bêtises, viennent de prototypo


